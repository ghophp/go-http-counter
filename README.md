go-http-counter [![Coverage Status](https://coveralls.io/repos/bitbucket/ghophp/go-http-counter/badge.svg?branch=master)](https://coveralls.io/bitbucket/ghophp/go-http-counter?branch=master)
===============

Go HTTP server that on each request responds with a counter of the total number of requests that it has received during the last 60 seconds. The server also return the correct numbers after restarting it, by persisting data to a file.

### Build [![CircleCI](https://circleci.com/bb/ghophp/go-http-counter/tree/master.svg?style=svg)](https://circleci.com/bb/ghophp/go-http-counter/tree/master)

```
make build
```

### Run

```
./bin/latest/go-http-counter

-path string
    path to create temporary files (default /tmp) (default "/tmp")
-port int
    port which server will listen (default 8080)
```

### Usage

Once the project is running, please request the root path `/` and check in the response for the header `X-Request-Count`.

### Dependencies

```
make deps
```

### Coverage

To see exactly the coverage into the code using the standard golang tool you can use the command bellow:

```
./script/coverage.sh --html
```