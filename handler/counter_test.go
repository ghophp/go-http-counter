package handler

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"
	"time"

	"github.com/op/go-logging"
)

const TestLoggerPrefix = "GO-HTTP-COUNTER-TEST"

type (
	MockStorage struct {
		current     []byte
		storeErr    error
		retrieveErr error
	}
)

var logger = logging.MustGetLogger(TestLoggerPrefix)

func NewMockStorage(current []byte, storeErr error, retrieveErr error) *MockStorage {
	return &MockStorage{current, storeErr, retrieveErr}
}

func (s *MockStorage) SetStoreError(err error) {
	s.storeErr = err
}

func (s *MockStorage) SetRetrieveError(err error) {
	s.retrieveErr = err
}

func (s *MockStorage) Store(value []byte) error {
	if s.storeErr != nil {
		return s.storeErr
	}
	s.current = value
	return nil
}

func (s *MockStorage) Retrieve() ([]byte, error) {
	if s.retrieveErr != nil {
		return nil, s.retrieveErr
	}
	return s.current, nil
}

func testRequestCode(t *testing.T, current, expected int) {
	if current != expected {
		t.Errorf("invalid response code %d", current)
	}
}

func testHeaderCount(t *testing.T, requestCount string, expected int) {
	count, err := strconv.Atoi(requestCount)
	if err != nil {
		t.Error("invalid request count")
	}

	if count != expected {
		t.Errorf("invalid request count %d", count)
	}
}

func TestCountShouldIncreaseEachRequest(t *testing.T) {
	var (
		content = []byte(`{"requests": []}`)
		handler = NewCounterHandler(logger, NewMockStorage(content, nil, nil))
		r, _    = http.NewRequest("GET", "/", nil)
		w       = httptest.NewRecorder()
	)

	handler.ServeHTTP(w, r)

	testRequestCode(t, w.Code, http.StatusOK)
	testHeaderCount(t, w.Header().Get(RequestCountHeader), 1)

	handler.ServeHTTP(w, r)

	testRequestCode(t, w.Code, http.StatusOK)
	testHeaderCount(t, w.Header().Get(RequestCountHeader), 2)
}

func TestCountShouldReturnToZeroAfter60Seconds(t *testing.T) {
	var (
		now     = time.Now()
		content = []byte(`{"requests": []}`)
		storage = NewMockStorage(content, nil, nil)
		handler = NewCounterHandler(logger, storage)
		r, _    = http.NewRequest("GET", "/", nil)
		w       = httptest.NewRecorder()
	)

	handler.ServeHTTP(w, r)

	testRequestCode(t, w.Code, http.StatusOK)
	testHeaderCount(t, w.Header().Get(RequestCountHeader), 1)

	now = now.AddDate(0, 0, -1)
	storage.Store([]byte(`{"requests": ["` + now.Format(time.RFC3339) + `"]}`))

	handler.ServeHTTP(w, r)

	testRequestCode(t, w.Code, http.StatusOK)
	testHeaderCount(t, w.Header().Get(RequestCountHeader), 1)
}

func TestErrorToRetrieveShouldNotResultInError(t *testing.T) {
	var (
		storage = NewMockStorage([]byte{}, nil, errors.New("error retrieving file"))
		handler = NewCounterHandler(logger, storage)
		r, _    = http.NewRequest("GET", "/", nil)
		w       = httptest.NewRecorder()
	)

	handler.ServeHTTP(w, r)

	testRequestCode(t, w.Code, http.StatusOK)
	testHeaderCount(t, w.Header().Get(RequestCountHeader), 1)
}

func TestToStoreMustReturnInternalServerError(t *testing.T) {
	var (
		storage = NewMockStorage([]byte{}, errors.New("error storing file"), nil)
		handler = NewCounterHandler(logger, storage)
		r, _    = http.NewRequest("GET", "/", nil)
		w       = httptest.NewRecorder()
	)

	handler.ServeHTTP(w, r)
	testRequestCode(t, w.Code, http.StatusInternalServerError)
}
