package handler

import (
	"encoding/json"
	"net/http"
	"strconv"
	"sync"
	"time"

	"github.com/op/go-logging"

	"bitbucket.org/ghophp/go-http-counter/storage"
)

const (
	// RequestCountHeader is the header used to inform the count
	RequestCountHeader = "X-Request-Count"
)

type (
	CounterHandler struct {
		logger  *logging.Logger
		storage storage.Storage
	}

	// Counter is marshal and unmarshal to the storage
	// and allows the logic into the handler to operate
	Counter struct {
		Requests []time.Time `json:"requests"`
	}
)

var mutex sync.Mutex

// NewCounterHandler return an instance of the CounterHandler
func NewCounterHandler(logger *logging.Logger, storage storage.Storage) *CounterHandler {
	return &CounterHandler{logger, storage}
}

// Fail sets the internal server error code into the response writer
// and also prompts to the logger instance
func (h *CounterHandler) Fail(w http.ResponseWriter, err error) {
	h.logger.Error(err)
	w.WriteHeader(http.StatusInternalServerError)
}

// ServeHTTP handles the request to incremental count in a 60 seconds timespan
// if the timespan has elapsed the count is reset to 0
func (h *CounterHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// due to IO access we have to make sure the requests are processed
	// one by one, to garatee both the consistency and the integrity
	mutex.Lock()

	stored, err := h.storage.Retrieve()
	if err != nil {
		stored = []byte{}
	}

	var currentCounter Counter
	if len(stored) > 0 {
		err = json.Unmarshal(stored, &currentCounter)
		if err != nil {
			mutex.Unlock()
			h.Fail(w, err)
			return
		}
	}

	newCounter := &Counter{[]time.Time{time.Now()}}
	for _, c := range currentCounter.Requests {
		elapsed := time.Since(c)
		if elapsed.Seconds() <= 60 {
			newCounter.Requests = append(newCounter.Requests, c)
		}
	}

	store, err := json.Marshal(newCounter)
	if err != nil {
		mutex.Unlock()
		h.Fail(w, err)
		return
	}

	err = h.storage.Store(store)
	if err != nil {
		mutex.Unlock()
		h.Fail(w, err)
		return
	}

	// once all the IO and computation is done, we can safelly
	// unlock the mutex and allow the next request to start
	mutex.Unlock()

	w.Header().Set(RequestCountHeader, strconv.Itoa(len(newCounter.Requests)))
	w.WriteHeader(http.StatusOK)
}
