package storage

import (
	"io/ioutil"
	"os"
)

type (
	// Storage defines how to interact with the storage
	Storage interface {
		Store([]byte) error
		Retrieve() ([]byte, error)
	}

	// FileStorage handle the storage based on the file system
	FileStorage struct {
		storagePath string
	}
)

// NewFileStorage return a instance of FileStorage or error
// if it is not possible to create the file in the path
func NewFileStorage(path string) (*FileStorage, error) {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		if _, err := os.Create(path); err != nil {
			return nil, err
		}
	}
	return &FileStorage{path}, nil
}

// Store stores the bytes into the file
func (s *FileStorage) Store(value []byte) error {
	err := ioutil.WriteFile(s.storagePath, value, 0644)
	if err != nil {
		return err
	}

	return nil
}

// Retrieve retrieves bytes from the file
func (s *FileStorage) Retrieve() ([]byte, error) {
	content, err := ioutil.ReadFile(s.storagePath)
	if err != nil {
		return nil, err
	}

	return content, nil
}
