package storage

import "testing"

func TestNonExistentPathShouldReturnError(t *testing.T) {
	_, err := NewFileStorage("/path/is/invalid")
	if err == nil {
		t.Error("expected err to not be nil for invalid path")
	}
}

func TestExistentPathShouldReturnInstance(t *testing.T) {
	_, err := NewFileStorage("/tmp/test-file")
	if err != nil {
		t.Error("expected err to be nil when creating instance")
	}
}

func TestStoreAndRetrieveShouldNotReturnError(t *testing.T) {
	store, err := NewFileStorage("/tmp/test-file")
	if err != nil {
		t.Error("expected err to be nil when creating instance")
	}

	err = store.Store([]byte("testcontent"))
	if err != nil {
		t.Error("expected err to be nil when storing value")
	}

	content, err := store.Retrieve()
	if err != nil {
		t.Error("expected err to be nil when retrieving value")
	}
	if string(content) != "testcontent" {
		t.Errorf("invalid value retrieved %s", string(content))
	}
}
