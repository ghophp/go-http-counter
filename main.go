package main

import (
	"flag"
	"fmt"
	"net/http"
	"path/filepath"

	"github.com/op/go-logging"

	"bitbucket.org/ghophp/go-http-counter/handler"
	"bitbucket.org/ghophp/go-http-counter/storage"
)

const (
	// DefaultPort holds the value of the default port for the http server
	DefaultPort = 8080

	// DefaultPath holds the value of the default path for storage
	DefaultPath = "/tmp"

	// DefaultStorageFile holds the value of the filename for storage
	DefaultStorageFile = "counter"

	// LoggerPrefix holds the value of the logger prefix
	LoggerPrefix = "GO-HTTP-COUNTER"
)

var logger = logging.MustGetLogger(LoggerPrefix)

// Log format string. Everything except the message has a custom color
// which is dependent on the log level. Many fields have a custom output
// formatting too, eg. the time returns the hour down to the milli second.
var format = logging.MustStringFormatter(
	`%{color}%{time:15:04:05.000} %{shortfunc} ▶ %{level:.4s} %{id:03x}%{color:reset} %{message}`,
)

func main() {
	logging.SetFormatter(format)

	var (
		port int
		path string
	)

	flag.IntVar(&port, "port", DefaultPort, "port which server will listen")
	flag.StringVar(&path, "path", DefaultPath, "path to create temporary files (default /tmp)")
	flag.Parse()

	filePath := filepath.Clean(path) + "/" + DefaultStorageFile
	fileStorage, err := storage.NewFileStorage(filePath)
	if err != nil {
		logger.Fatal(err)
	}

	http.Handle("/", handler.NewCounterHandler(logger, fileStorage))
	http.ListenAndServe(fmt.Sprintf(":%d", port), nil)
}
