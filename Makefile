.PHONY: all deps test clean build coveralls

GO ?= go
BIN_NAME=go-http-counter

all: build test

deps:
	${GO} get github.com/op/go-logging

build: deps
build:
	${GO} build -o bin/latest/${BIN_NAME}

test: deps
test:
	${GO} test bitbucket.org/ghophp/go-http-counter/handler
	${GO} test bitbucket.org/ghophp/go-http-counter/storage

coveralls:
	./script/coverage.sh --coveralls

clean:
	rm -rf bin/latest/${BIN_NAME}